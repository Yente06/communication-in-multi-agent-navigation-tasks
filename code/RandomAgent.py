
import random


class RandomAgent:
    
    def __init__(self, action_space) -> None:
        self.action_space = action_space
        
    def act(self) -> int:
        return random.randint(0,self.action_space-1)