

from matplotlib import pyplot as plt
from pandas import read_csv


def plot_reward(filenames,labels,title, save_name,show=False):
	dataframes = {labels[i]: read_csv(filenames[i]) for i in range(len(filenames))}
	
	for label, df in dataframes.items():
		x = df['Step']/10000
		y = df['Average reward']
		plt.plot(x, y, label=label)
  
	plt.legend()
	plt.xlim([0,25])
	plt.title(title)
	plt.xlabel('Step')
	plt.ylabel('Average reward')
	plt.savefig(save_name)
	if show:plt.show()
	plt.close()
	
if __name__ == '__main__':
	grids = ["empty",
         # "flower","four_room","pong","two_room"
          ]
	for grid in grids:
		plot_reward(filenames=[
			"results_Single_QLAgent_2_"+grid,
			"results_Single_Random_"+grid,
			"results_SARSA_3_"+grid
		],
				labels=["QL Agent","Random Agent","SARSA Agent"],
				title=grid,
				save_name=f"plot_result_2_{grid}",
				show=False)