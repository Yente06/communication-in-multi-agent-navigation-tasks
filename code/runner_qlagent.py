import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from pandas import DataFrame

from Sender import Sender
from Receiver import Receiver
from Environment import Pong, Empty_room, Flower, Two_room, Four_room
from QLAgent import QLAgent
from Environment import Environment
from RandomAgent import RandomAgent

def run_simple_qlagent(env_func:callable(Environment),env_name:str) -> None:

 
	env = env_func()
	gamma = 0.9
	n_states = env.num_states
	n_actions = env.num_actions
	num_episodes = 100000
	evaluate_every = 1000
	num_evaluation_episodes = 100
	learning_rate = 0.01
	epsilon = 1
	epsilon_min = 0.1
	epsilon_decay = 0.99999
	p_term = 1-gamma
	qlagent = QLAgent(n_states, n_actions, gamma, learning_rate,
					  epsilon, epsilon_min, epsilon_decay)

	evaluations_rewards = []

	for episode in range(num_episodes):

		r = run_episode_qlagent(env, qlagent, True, p_term)
		# print("Episode: {}, Reward: {}".format(episode, r))
		# print(env)
		# print("%%%%%%%%%%%%%%%%%%")
		if (episode+1) % evaluate_every == 0:
			eval_rewards = 0
			for eval in range(num_evaluation_episodes):
				eval_rewards += run_episode_qlagent(env, qlagent, False, p_term)
				# print(f"episode {episode}, evaluation reward : {eval_rewards} ")
			evaluations_rewards.append([episode+1, eval_rewards/num_evaluation_episodes])
			print("Episode: {}, Evaluation: {}".format(evaluations_rewards[-1][0],evaluations_rewards[-1][1]))
   
			print("saving")
			DataFrame(np.array(evaluations_rewards), columns=[
			          "Step", "Average reward"]).to_csv("results_Single_QLAgent_3_"+env_name)
   
	print("Done, saving ...")
	DataFrame(np.array(evaluations_rewards), columns=[
			"Step", "Average reward"]).to_csv("results_Single_QLAgent_3_"+env_name)


def run_episode_qlagent(env:Environment, qlagent:QLAgent, is_training:bool,p_term:float) -> float:
	""" 
 	Run a single episode of the Q-learning algorithm.
	Args:
		env: The OpenAI environment.
		qlagent: The Q-learning agent.
		is_training: Whether the agent is training or evaluating.
		gamma: Discount factor.
		p_term: Probability of termination.
	Returns: The total reward of the episode.
	"""
	
	done = False
	state = env.reset()
	goal = env.goal()
	cum_reward = 0.
	
	while not done:
		
		action = qlagent.act(state,True,goal)
		reward, next_state, done = env.step(action)
		if (np.random.rand() <= p_term):
			done = True
		if is_training:
			qlagent.learn(state, action, reward, done, next_state, goal)
		state = next_state
		cum_reward += reward
		
			
  
	return cum_reward

def run_random_agent(env_func:callable(Environment), env_name:str) -> None:

	env = env_func()
	gamma = 0.9
	num_episodes = 600_000
	evaluate_every = 2000
	num_evaluation_episodes = 100
	p_term = 1-gamma
	agent = RandomAgent(env.num_actions)

	evaluations_rewards = []
	for episode in range(num_episodes):

		if (episode+1) % evaluate_every == 0:
			eval_rewards = []
			for eval in range(num_evaluation_episodes):
				done = False
				env.reset()
				while not done:
					action = agent.act()
					reward, next_state, done = env.step(action)
					if (np.random.rand() <= p_term):
						done = True
				eval_rewards.append(reward)

			evaluations_rewards.append([episode+1, np.mean(eval_rewards)/num_evaluation_episodes])
			print("Episode: {}, Evaluation: {}".format(*evaluations_rewards[-1]))

		
  
	print("Done, saving ...")
	DataFrame(np.array(evaluations_rewards), columns=[
			"Step", "Average reward"]).to_csv("results_Single_Random_2_"+env_name)


if __name__ == "__main__":
	
	worlds = {
            # "empty": Empty_room,
            # "pong": Pong,
			"four_room": Four_room,
			# "two_room": Two_room,
			# "flower": Flower, 
   }

	for world_name, world_func in worlds.items():
		print("Running {}".format(world_name))
		print("-"*20)
		run_simple_qlagent(world_func, world_name)
		#run_random_agent(world_func, world_name)
		print("-"*20)
		print(f"Done, {world_name}")
	