import numpy as np

class Environment:
    def __init__(
        self,
        matrix):
        
        if (matrix.shape[0] % 2 == 0 or matrix.shape[0] % 2 == 0): raise Exception('Environment should be of shape: odd x odd, found even x even')
        
        self.matrix = matrix
        self.num_actions = 4
        self.num_states = self.matrix.shape[0] * self.matrix.shape[1]
        self.reset()

    def reset(self): # Resets the environment with a new random goal state
        self.pos    = [ int((self.matrix.shape[0] - 1)/2), int((self.matrix.shape[1] - 1)/2) ]
        self.goal_state = False
        while (not self.goal_state or self.pos == self.goal_state or self.matrix[self.goal_state[0], self.goal_state[1]] == 'W'):
            self.goal_state = [ np.random.randint(self.matrix.shape[0]), np.random.randint(self.matrix.shape[0])]

        return self.state()
    
    def state(self): # Returns the sate as a one-hot vector representation
        return self.pos[0] * self.matrix.shape[1] + self.pos[1]

    def goal(self): # Returns the goal as a one-hot vector representation
        return self.goal_state[0] * self.matrix.shape[1] + self.goal_state[1]

    def step(self, action): # Takes action and returns new reward and state

        new_pos = [ self.pos[0], self.pos[1] ] # [ y, x ]

        if (action == 0): new_pos[0] +=1 # Up
        if (action == 1): new_pos[1] +=1 # Right
        if (action == 2): new_pos[0] -=1 # Down
        if (action == 3): new_pos[1] -=1 # Left

        reward = 0
        done = False

        if (new_pos[0] >= 0 and new_pos[1] >= 0 and new_pos[0] < self.matrix.shape[0] and new_pos[1] < self.matrix.shape[1]):

            state = self.matrix[new_pos[0], new_pos[1]]

            if (state != 'W'): self.pos = new_pos # Didn't hit a wall
            
            if (new_pos == self.goal_state):
                reward = 1
                done = True

        return reward, self.state(), done

    def print(self):
        for row in range(self.matrix.shape[0]):
            for col in range(self.matrix.shape[1]):
                if ([row,col] == self.goal_state):
                    print('G', end='')
                elif ([row,col] == self.pos):
                    print('+', end='')
                elif (self.matrix[row][col] == 'W'):
                    print('#', end='')
                else:
                    print('_', end='')
            print()

class Pong(Environment):
    def __init__(
        self):
        super().__init__(
            np.array([
                [' ',' ','W',' ',' '],
                ['W',' ','W',' ','W'],
                ['W',' ',' ',' ','W'],
                ['W',' ','W',' ','W'],
                [' ',' ','W',' ',' '],
            ])
        )

class Four_room(Environment):
    def __init__(
        self):
        super().__init__(
            np.array([
                [' ',' ','W',' ',' '],
                [' ',' ',' ',' ',' '],
                ['W','W',' ','W','W'],
                [' ',' ',' ',' ',' '],
                [' ',' ','W',' ',' '],
            ])
        )

class Two_room(Environment):
    def __init__(
        self):
        super().__init__(
            np.array([
                [' ',' ','W',' ',' '],
                [' ',' ','W',' ',' '],
                [' ',' ',' ',' ',' '],
                [' ',' ','W',' ',' '],
                [' ',' ','W',' ',' '],
            ])
        )

class Flower(Environment):
    def __init__(
        self):
        super().__init__(
            np.array([
                [' ',' ','W',' ',' '],
                [' ',' ',' ',' ',' '],
                ['W',' ',' ',' ','W'],
                [' ',' ',' ',' ',' '],
                [' ',' ','W',' ',' '],
            ])
        )

class Empy_room(Environment):
    def __init__(
        self):
        super().__init__(
            np.array([
                [' ',' ',' ',' ',' '],
                [' ',' ',' ',' ',' '],
                [' ',' ',' ',' ',' '],
                [' ',' ',' ',' ',' '],
                [' ',' ',' ',' ',' '],
            ])
        )