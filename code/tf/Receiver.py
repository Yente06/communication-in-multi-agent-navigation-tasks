import numpy as np
import tensorflow as tf

def one_hot_vector(state, messages, nb_states, C):
    arr = np.zeros(nb_states + C)
    arr[state] = 1
    for message in messages:
        arr[nb_states + message] = 1
    return arr

def construct_q_network(state_dim, action_dim):
    """Construct the q-network with q-values per action as output"""
    inputs = tf.keras.layers.Input(shape=(state_dim,1))  # input dimension
    hidden1 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(inputs)
    #hidden2 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden1)
    #hidden3 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden2)
    q_values = tf.keras.layers.Dense(action_dim, kernel_initializer=tf.keras.initializers.Zeros(), activation="linear")(hidden1)#(hidden3)

    q_network = tf.keras.Model(inputs=inputs, outputs=[q_values])

    return q_network

class Receiver:
    def __init__(
        self,
        C,
        nb_states,
        nb_actions,
        gamma,
        epsilon,
        learning_rate):
        """
            C: channel capacity
            nb_states: Amount of possible states
            nb_actions: Amount of possible messages
            gamma: Discount factor
            epsilon: Epsilon of epsilon-greedy
        """
        self.C = C
        self.nb_states = nb_states
        self.nb_actions = nb_actions
        self.gamma = gamma
        self.epsilon = epsilon
        self.Q = construct_q_network(nb_states + C, nb_actions)
        #self.opt = tf.keras.optimizers.experimental.SGD(learning_rate=learning_rate) # Optimizer
        self.opt = tf.keras.optimizers.RMSprop(learning_rate=learning_rate) # Optimizer

    def act(self, state, messages, training):
        
        if (training and np.random.rand() <= self.epsilon):
            # Choose randomly
            return np.random.randint(self.nb_actions)
        else:
            # Choose greedy
            return self.greedy_action(state, messages)
    
    def greedy_action(self, state, messages):
        q_values = self.Q(tf.expand_dims(one_hot_vector(state, messages, self.nb_states, self.C), axis=1))
        return np.argmax(q_values[0, 0])

    def loss_value(self, state, action, reward, next_state, messages, t, T):
        q_values = self.Q(tf.expand_dims(one_hot_vector(state, messages, self.nb_states, self.C), axis=1)) # No gradient computation
        q_value = q_values[0, 0, action]
        "Select next action with highest Q-value"
        if t == 0:
            next_q_value = 0 # No Q-value for terminal
        else:
            next_q_values = tf.stop_gradient(self.Q(tf.expand_dims(one_hot_vector(state, messages, self.nb_states, self.C), axis=1))) # No gradient computation
            next_action = np.argmax(next_q_values[0, 0])
            next_q_value = next_q_values[0, 0, next_action]

        "Compute observed Q-value"
        observed_q_value = reward + (self.gamma * next_q_value)

        "Compute loss value"
        current_q_value = q_value
        loss_value = (observed_q_value-current_q_value)**2
        
        """
        q_values = tf.stop_gradient(self.Q(tf.expand_dims(one_hot_vector(state, messages, self.nb_states, self.C), axis=1))) # No gradient computation
        q_value = q_values[state, 0, action]

        if (t >=0 and t <= T):
            next_q_values = tf.stop_gradient(self.Q(tf.expand_dims(one_hot_vector(next_state, messages, self.nb_states, self.C), axis=1))) # No gradient computation
            next_action = np.argmax(next_q_values[0, 0])
            next_q_value = next_q_values[0, 0, next_action]

            loss_value = ( reward + self.gamma * next_q_value - q_value ) ** 2
        else:
            loss_value = q_value * 0.0
        """
        

        return loss_value

    def learn(self, loss_value, tape):
        "Compute gradients"
        grads = tape.gradient(loss_value, self.Q.trainable_variables)

        "Apply gradients to update network weights"
        self.opt.apply_gradients(zip(grads, self.Q.trainable_variables))

        
