import numpy as np
import tensorflow as tf

def one_hot_vector(state, nb_states):
    arr = np.zeros(nb_states)
    arr[state] = 1
    return arr

def construct_q_network(state_dim, action_dim):
    """Construct the q-network with q-values per action as output"""
    inputs = tf.keras.layers.Input(shape=(state_dim,1))  # input dimension
    hidden1 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(inputs)
    #hidden2 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden1)
    #hidden3 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden2)
    q_values = tf.keras.layers.Dense(action_dim, kernel_initializer=tf.keras.initializers.Zeros(), activation="linear")(hidden1)#(hidden3)

    q_network = tf.keras.Model(inputs=inputs, outputs=[q_values])

    return q_network

class Sender:
    def __init__(
        self,
        nb_states,
        nb_actions,
        gamma,
        epsilon,
        learning_rate):
        """
            nb_states: Amount of possible states
            nb_actions: Amount of possible messages
            epsilon: Epsilon of epsilon-greedy
        """

        self.nb_actions = nb_actions
        self.nb_states = nb_states
        self.gamma = 0
        self.epsilon = epsilon
        self.Q = construct_q_network(nb_states, nb_actions)
        self.tape = tf.GradientTape()
        #self.opt = tf.keras.optimizers.experimental.SGD(learning_rate=learning_rate) # Optimizer
        self.opt = tf.keras.optimizers.RMSprop(learning_rate=learning_rate) # Optimizer

    def act(self, state, training):
        
        if (training and np.random.rand() <= self.epsilon):
            # Choose randomly
            return np.random.randint(self.nb_actions)
        else:
            # Choose greedy
            return self.greedy_action(state)

    
    def greedy_action(self, state):
        q_values = self.Q(tf.expand_dims(one_hot_vector(state, self.nb_states), axis=1))
        return np.argmax(q_values[0, 0])

    def loss_value(self, state, action, reward, t, T):
        q_values = self.Q(tf.expand_dims(one_hot_vector(state, self.nb_states), axis=1)) # No gradient computation
        q_value = q_values[state, 0, action]
        "Select next action with highest Q-value"
        if t == T:
            next_q_values = tf.stop_gradient(self.Q(tf.expand_dims(one_hot_vector(state, self.nb_states), axis=1))) # No gradient computation
            next_action = np.argmax(next_q_values[0, 0])
            next_q_value = next_q_values[0, 0, next_action]
        else:
            next_q_value = 0 # No Q-value for terminal

        "Compute observed Q-value"
        observed_q_value = reward + (self.gamma * next_q_value)

        "Compute loss value"
        current_q_value = q_value
        loss_value = (observed_q_value-current_q_value)**2


        """
        q_values = tf.stop_gradient(self.Q(tf.expand_dims(one_hot_vector(state, self.nb_states), axis=1))) # No gradient computation
        q_value = q_values[state, 0, action]

        if (t == T):
            loss_value = ( reward - q_value ) ** 2
        else:
            loss_value = q_value * 0.0
        """

        return loss_value

    def learn(self, loss_value, tape):
        "Compute gradients"
        grads = tape.gradient(loss_value, self.Q.trainable_variables)

        "Apply gradients to update network weights"
        self.opt.apply_gradients(zip(grads, self.Q.trainable_variables))