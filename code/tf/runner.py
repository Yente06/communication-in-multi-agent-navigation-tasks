import numpy as np
import tensorflow as tf

from Sender import Sender
from Receiver import Receiver
from Environment import Pong

print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

def run_episode(senders, receiver, environment, p_term, learning_rate, training = True):
    
    #tape = tf.GradientTape()
    #opt = tf.keras.optimizers.experimental.SGD(learning_rate=learning_rate) # Optimizer
    
    state = environment.reset()
    messages = [ sender.act(state, training) for sender in senders ]
    action = receiver.act(state, messages, training)
    
    T = -2
    t = 0

    total_reward = 0

    with tf.GradientTape(persistent=True) as tape_s, tf.GradientTape(persistent=True) as tape_r:
        while (t > T+1): # Hack to use the same notation as in the paper, a variable called done would be more readable
            reward, next_state, done = environment.step(action)
            if (done or np.random.rand() <= p_term): T = t

            loss_value_s = tf.reduce_sum([ sender.loss_value(environment.goal(), messages[i], reward, t, T) for i, sender in enumerate(senders) ])
            loss_value_r = receiver.loss_value(state, action, reward, next_state, messages, t, T)

            loss_value = loss_value_s + loss_value_r

            # Teach senders
            for sender in senders:
                sender.learn(loss_value, tape_s)

            # Teach receiver
            receiver.learn(loss_value, tape_r)

            state = next_state
            action = receiver.act(state, messages, training)

            total_reward += reward
            t += 1
            #environment.print()

    return total_reward # To stop the loop we set T equal to t, so t will always be equal to the amount of loops executed


def train(
    environment,
    M,
    C,
    learning_reate_RMSprop,
    epsilon_s,
    epsilon_r,
    gamma,
    num_episodes,
    evaluate_every,
    num_evaluation_episodes
    ):

    N = int(C ** (1/M))

    senders = [ Sender(environment.num_states, N, gamma, epsilon_s, learning_reate_RMSprop) for _ in range(M) ]         # Sender: env state -> 1 of N messages
    receiver = Receiver(C, environment.num_states, environment.num_actions, gamma, epsilon_r, learning_reate_RMSprop) # Receiver: env state + messages -> 1 of 4 actions

    digits = len(str(num_episodes))
    evaluation_returns = np.zeros(num_episodes // evaluate_every)
    returns = np.zeros(num_episodes)

    p_term = 1 - gamma

    for episode in range(num_episodes):
        returns[episode] = run_episode(senders, receiver, environment, p_term, learning_reate_RMSprop, True)

        if (episode + 1) % evaluate_every == 0:
            evaluation_step = episode // evaluate_every
            rewards_eval = np.zeros(num_evaluation_episodes)
            for eval_episode in range(num_evaluation_episodes):
                rewards_eval[eval_episode] = run_episode(senders, receiver, environment, p_term, learning_reate_RMSprop, False)
            evaluation_returns[evaluation_step] = np.mean(rewards_eval)

            print(f"Episode {(episode + 1): >{digits}}/{num_episodes:0{digits}}:\t"
                  f"Averaged evaluation return {evaluation_returns[evaluation_step]:0.3}")

    return senders, receiver, returns, evaluation_returns

if __name__ == '__main__':
    environment = Pong()
    senders, receiver, returns, evaluation_returns = train(
        environment = environment,
        M = 2,
        C = 9,
        learning_reate_RMSprop = 1e-4,
        epsilon_s = 0.1,
        epsilon_r = 0.1,
        gamma = 0.8,
        num_episodes = 10000,
        evaluate_every = 100,
        num_evaluation_episodes = 50
    )