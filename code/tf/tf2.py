# https://towardsdatascience.com/a-minimal-working-example-for-deep-q-learning-in-tensorflow-2-0-e0ca8a944d5e

import tensorflow as tf
import numpy as np
from Environment import Pong

def one_hot_vector(v, length):
    arr = np.zeros(length)
    arr[v] = 1
    return arr

def construct_q_network(state_dim, action_dim):
    """Construct the q-network with q-values per action as output"""
    inputs = tf.keras.layers.Input(shape=(state_dim,1))  # input dimension
    hidden1 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(inputs)
    hidden2 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden1)
    hidden3 = tf.keras.layers.Dense(25, activation="relu", kernel_initializer=tf.keras.initializers.he_normal())(hidden2)
    q_values = tf.keras.layers.Dense(action_dim, kernel_initializer=tf.keras.initializers.Zeros(), activation="linear")(hidden3)

    q_network = tf.keras.Model(inputs=inputs, outputs=[q_values])

    return q_network


env = Pong()
q_network = construct_q_network(env.num_states, env.num_actions)
epsilon = 0.2
gamma = 0.99
opt = tf.keras.optimizers.experimental.SGD(learning_rate=0.1) # Optimizer

state = env.reset()
for episode in range(env.num_states):
    with tf.GradientTape() as tape:
        "Obtain Q-values from network"
        q_values = q_network(tf.expand_dims(one_hot_vector(state, env.num_states), axis=1))
        # q_values shape=(25, 1, 4) 25 input, 4 output, 1 as dim expansion (ignore)

        "Select action using epsilon-greedy policy"
        sample_epsilon = np.random.rand()
        if sample_epsilon <= epsilon: # Select random action
            action = np.random.choice(env.num_actions)
        else: # Select action with highest Q-value
            action = np.argmax(q_values[0, 0])

        "Obtain direct reward for selected action"
        reward, next_state, done = env.step(action)

        "Obtain Q-value for selected action"
        q_value = q_values[0, 0, action]

        "Select next action with highest Q-value"
        if next_state == env.goal():
            next_q_value = 0 # No Q-value for terminal
            print("yaas")
        else:
            next_q_values = tf.stop_gradient(q_network(tf.expand_dims(one_hot_vector(state, env.num_states), axis=1))) # No gradient computation
            next_action = np.argmax(next_q_values[0, 0])
            next_q_value = next_q_values[0, 0, next_action]

        "Compute observed Q-value"
        observed_q_value = reward + (gamma * next_q_value)

        "Compute loss value"
        current_q_value = q_value
        loss_value = (observed_q_value-current_q_value)**2

        "Compute gradients"
        print("Loss")
        print(loss_value)
        print("Vars")
        print(q_network.trainable_variables)
        grads = tape.gradient(loss_value, q_network.trainable_variables)

        "Apply gradients to update network weights"
        opt.apply_gradients(zip(grads, q_network.trainable_variables))

        "Update state"
        state = next_state