from random import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from pandas import DataFrame

from Sender import Sender
from Receiver import Receiver
from Environment import Pong, Empty_room, Flower, Two_room, Four_room


def run_episode(receiver, environment, messages, p_term, training=True):
	done = False
	state = environment.state()
	action = receiver.act(state, messages, training)
	while not done:
		reward, next_state, done = environment.step(action)
		next_action = receiver.act(next_state, messages, training)

		if (training):
			receiver.learn(state, action, reward, next_state,
						   next_action, messages, done)
		state = next_state
		action = next_action

		if (done):
			return reward
		if (np.random.rand() <= p_term):
			return reward


def train_agents(M):
	"""
		M: Number of sender agents
	"""
	C = 64  # C: Communication chanel capacity (N^M)
	learning_rate_s = learning_rate_r = 5e-5 # learning_rate_r: Learning rate of receivers
	gamma_s = gamma_r = 0.9  # gamma_s: Sender's Q-learning discount factor
	epsilon_max_s = epsilon_max_r = 1 # epsilon_max_s: Sender's maximum action exploration rate
	# epsilon_min_s: Sender's minimum action exploration rate
	epsilon_min_s = epsilon_min_r = .0
	# epsilon_decay_r: Receiver's action exploration rate decay
	epsilon_decay_s = epsilon_decay_r = 0.99975
 
	warmup_s = warmup_r = 0
	environment = Flower()
	nb_outer_episodes = 100
	nb_inner_episodes = 100
	nb_messages = int(C ** (1 / M))
	p_term = 1 - gamma_r

	senders = [Sender(environment.num_states, nb_messages, learning_rate_s, gamma_s,
					  epsilon_max_s, epsilon_min_s, epsilon_decay_s, warmup_s) for _ in range(M)]
	receiver = Receiver(M, nb_messages, environment.num_states, 4, learning_rate_r,
						gamma_r, epsilon_max_r, epsilon_min_r, epsilon_decay_r, warmup_r)


	for episode in range(nb_outer_episodes):
		avg_reward = 0

		# Train senders for a couple of episodes on the current most optimal receiver
		for inner_episode in range(nb_inner_episodes):
			environment.reset()
			goal = environment.goal()
			# Get sender outputs
			messages = [sender.act(goal, True) for sender in senders]
			# Get reward from trained receiver (don't train receiver)
			reward = run_episode(receiver, environment, messages, p_term, False)
			# Give reward to senders
			for i, sender in enumerate(senders):
				sender.learn(goal, messages[i], reward, True)

			avg_reward += reward

		# Train receiver for a couple of episodes on the current most optimal senders
		for inner_episode in range(nb_inner_episodes):
			environment.reset()
			goal = environment.goal()
			# Get sender outputs (don't train)
			messages = [sender.act(goal, False) for sender in senders]
			# Train receiver
			reward = run_episode(receiver, environment, messages, p_term, True)

			avg_reward += reward

	return senders, receiver, environment


def evaluate_individual_sender_impact():
	# Train the sender and receiver agents using the Q-learning algorithm
	sender_agents, receiver_agent, env = train_agents(M=5)
	gamma_r = 0.9
	p_term = 1- gamma_r
 
	# Evaluate the agents' performance by letting them perform the task for 1000 episodes, with e=0 for both agents
	eval_returns = []
	for i in range(1000):
		state = env.reset()
		episode_return = 0
		goal = env.goal()
		messages = [sender.act(goal,False) for sender in sender_agents]
		episode_return = run_episode(receiver_agent, env, messages,p_term, False)
		eval_returns.append(episode_return)

	# Compute the baseline performance
	baseline_performance = sum(eval_returns) # / len(eval_returns)

	# Evaluate the relative impact of individual senders on the task performance
	sender_performance = {}
	for sender_idx, sender in enumerate(sender_agents):
		returns = []
		performance_drop = 0
		for i in range(1000):
			state = env.reset()
			done = False
			goal = env.goal()
			messages = [sender.act(goal, False) for sender in sender_agents]
			# Replace the message from this sender with a random message from its vocabulary
			messages[sender_idx] = np.random.randint(sender.nb_actions)
			
			episode_return = 0
			action = receiver_agent.act(state, messages, False)

			while not done:
				reward, next_state, done = env.step(action)
				next_action = receiver_agent.act(next_state, messages, False)
				action = next_action
				if (np.random.rand() <= p_term):
					done = True

				episode_return += reward
			returns.append(episode_return)
  
		# Compute the drop in performance relative to the baseline
		performance_drop = (sum(returns) - baseline_performance
		                    ) / baseline_performance
		sender_performance[sender_idx] = performance_drop*100
	
	print("Baseline performance: ", baseline_performance)
	sorted_returns = sorted(sender_performance.items(), key=lambda x: x[1], reverse=True)
	print("Sender performance drop: ", sorted_returns)
 
	# Plot the sender performance drop against the sender index
	for sender, performance_drop in sorted_returns:
		print(f"Sender {sender} performance drop: {performance_drop:0.4}%")
		plt.bar(sender,performance_drop*-1,)
	plt.xlabel("Sender index")
	plt.ylabel("Performance drop (%)")
	plt.legend()
	plt.show()
 
if __name__ == "__main__":
	evaluate_individual_sender_impact()

