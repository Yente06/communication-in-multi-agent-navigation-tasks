
import numpy as np


class QLAgent:
	"""
	Basic Q-learning agent
	"""
 
	def __init__(self, states : int, actions: int, gamma:float = 1,learning_rate:float=0.01, e_max:float=0.9, e_min:float=0.9, e_decay:float=0.995):
		"""
		Args:
			states (int): Number of states
			actions (int): Number of actions
			gamma (float, optional): Discount factor. Defaults to 1.
			learning_rate (float, optional): Learning rate. Defaults to 0.01.
			e_max (float, optional): Exploration factor max. Defaults to 0.9.
			e_min (float, optional): Exploration factor min. Defaults to 0.9.
			e_decay (float, optional): Exploration factor decay. Defaults to 0.995.
		"""
		self.states = states
		self.actions = actions
		self.gamma = gamma            # discount factor
		self.learning_rate = learning_rate # learning rate
		self.epsilon = e_max            # exploration factor (max)
		self.e_min = e_min            # exploration factor (min)
		self.e_decay = e_decay        # exploration factor decay
		self.q_table = np.zeros((states,states, actions)) # Q table (state, action) 
  

	def greedy_action(self,observation:int,goal:int)-> int:
		"""
		Select the greedy action for the given state.
		Args:
			observation (int): observation
		Returns:
			int: Greedy action for the given state.
		"""
		return np.argmax(self.q_table[observation,goal])

	def act(self, observation:np.ndarray,training:bool,goal)-> int:
		"""
		Select an action for the given state.
		Args:
			observation (np.ndarray): observation array.
			training (bool, optional): Training flag. Defaults to True. this is used to decide whether to use epsilon-greedy or not.
		Returns:
			int: Selected action.
		"""
		res = 0
		if training:
			if np.random.rand() < self.epsilon:
				res = np.random.randint(self.actions)
			else:
				res =  self.greedy_action(observation,goal)
		else:
			res = self.greedy_action(observation,goal)
		return res

	def learn(self, observation:np.ndarray, action:int, reward:float, done:float, next_observation:np.ndarray,goal)->None:
		""" Update Q-value

		Args:
			observation (np.ndarray): observation array.           
			action (int): action.
			action (int): action
			reward (float): reward
			done (bool): done flag. True if the episode is finished, False otherwise.
			next_observation (np.ndarray): next observation array.           
		"""

		self.q_table[observation, goal, action] += self.learning_rate * (reward + self.gamma * np.max(
			self.q_table[next_observation, goal]) - self.q_table[observation, goal, action])
		if done:
			self.epsilon = max(self.e_min, self.e_decay * self.epsilon)
  