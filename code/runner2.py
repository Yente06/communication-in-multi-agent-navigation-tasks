import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from pandas import DataFrame

from Sender import Sender
from Receiver import Receiver
from Environment import Pong, Empty_room, Flower, Two_room, Four_room


def run_episode(receiver, environment, messages, p_term, training=True):
	done = False
	state = environment.state()
	action = receiver.act(state, messages, training) 
	while not done:
		reward, next_state, done = environment.step(action)
		next_action = receiver.act(next_state, messages, training)

		if(training): receiver.learn(state, action, reward, next_state, next_action, messages, done)
		state = next_state
		action = next_action

		if (done): return reward
		if (np.random.rand() <= p_term): return reward


def run(M,
		C,
		learning_rate_s,
		learning_rate_r,
		gamma_s,
		gamma_r,
		epsilon_max_s,
		epsilon_max_r,
		epsilon_min_s,
		epsilon_min_r,
		epsilon_decay_s,
		epsilon_decay_r,
		warmup_s,
		warmup_r,
		environment,
		nb_outer_episodes,
		nb_inner_episodes,
		nb_evaluation_episodes):
	"""
		M: Number of sender agents
		C: Communication chanel capacity (N^M)
		learning_rate_s: Learning rate of senders
		learning_rate_r: Learning rate of receivers
		gamma_s: Sender's Q-learning discount factor
		gamma_r: Receiver's Q-learning discount factor
		epsilon_max_s: Sender's maximum action exploration rate
		epsilon_max_r: Receiver's maximum action exploration rate
		epsilon_min_s: Sender's minimum action exploration rate
		epsilon_min_r: Receiver's minimum action exploration rate
		epsilon_decay_s: Sender's action exploration rate decay
		epsilon_decay_r: Receiver's action exploration rate decay
		warmup_s: The amount of rounds for which the senders will select a rendom action in the beginning
		warmup_r: The amount of rounds for which the receiver will select a rendom action in the beginning
		environment: The envorinment
		nb_episodes_s: The total amount of episodes to train for
		evaluate_every: The evaluation interval (on the outer loop of the training process)
		nb_evaluation_episodes: Number of episodes per evaluation
	"""

	nb_messages = int(C ** (1 / M))
	p_term = 1 - gamma_r

	senders = [ Sender(environment.num_states, nb_messages, learning_rate_s, gamma_s, epsilon_max_s, epsilon_min_s, epsilon_decay_s, warmup_s) for _ in range(M) ]
	receiver = Receiver(M, nb_messages, environment.num_states, 4, learning_rate_r, gamma_r, epsilon_max_r, epsilon_min_r, epsilon_decay_r, warmup_r)
	
	eval_rewards = np.zeros(nb_outer_episodes)

	for episode in range(nb_outer_episodes):
		avg_reward = 0

		# Train senders for a couple of episodes on the current most optimal receiver
		for inner_episode in range(nb_inner_episodes):
			environment.reset()
			goal = environment.goal()
			# Get sender outputs
			messages = [ sender.act(goal, True) for sender in senders ]
			# Get reward from trained receiver (don't train receiver)
			reward = run_episode(receiver, environment, messages, p_term, False)
			# Give reward to senders
			for i, sender in enumerate(senders):
				sender.learn(goal, messages[i], reward, True)

			avg_reward += reward

		# Train receiver for a couple of episodes on the current most optimal senders
		for inner_episode in range(nb_inner_episodes):
			environment.reset()
			goal = environment.goal()
			# Get sender outputs (don't train)
			messages = [ sender.act(goal, True) for sender in senders ]
			# Train receiver
			reward = run_episode(receiver, environment, messages, p_term, True)

			avg_reward += reward

		# AVG trainig reward
		#print(f"Average trainig reward: {avg_reward / (nb_inner_episodes * 2)}")

		# AVG evaluating reward
		eval_reward = 0
		for eval in range(nb_evaluation_episodes):
			environment.reset()
			goal = environment.goal()
			messages = [ sender.act(goal, False) for sender in senders ]
			eval_reward += run_episode(receiver, environment, messages, p_term, False)
		
		eval_rewards[episode] = eval_reward / nb_evaluation_episodes
		   
		print(f"Average evaluation reward: {eval_rewards[episode]} (episode: {episode})")

	return eval_rewards, senders, receiver
		

 
def display_sender_policy(sender, environment):
	c = [ c for c in colors.CSS4_COLORS if not c == 'black' ]
	np.random.shuffle(c)
	c = np.append([ 'black' ], c)
	cmap = colors.ListedColormap(c)

	bounds = [ i for i in range(sender.nb_actions + 2) ]
	norm = colors.BoundaryNorm(bounds, cmap.N)

	fig, ax = plt.subplots()

	nb_rows = environment.matrix.shape[0]
	nb_cols = environment.matrix.shape[1]

	policy = np.zeros((nb_rows, nb_cols), dtype=int)

	# Get policy
	for i in range(nb_rows):
		for j in range(nb_cols):
			if (environment.matrix[i, j] == 'W'):
				policy[i, j] = 0
			else:
				goal = i*nb_cols + j
				policy[i, j] = sender.act(goal, False) + 1

	ax.imshow(policy, cmap=cmap, norm=norm)
	ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
	ax.set_xticks(np.arange(-0.5, nb_cols, 1))
	ax.set_yticks(np.arange(-0.5, nb_rows, 1))

	return policy - 1

def display_receiver_policy(receiver, policy):
	nb_rows = policy.shape[0]
	nb_cols = policy.shape[1]

	messages = [ m for m in np.unique(policy) if m >= 0 ] # Filter out the walls

	print(messages)

	for message in messages:
		cmap = colors.ListedColormap(['black', 'white', 'blue'])
		filtered_policy = np.zeros(policy.shape, dtype=int)

		# Indicate message on grid
		for (i,j),m in np.ndenumerate(policy):
			if (m == message):
				filtered_policy[i, j] = 2
			elif (m == -1):
				filtered_policy[i, j] = 0
			else:
				filtered_policy[i, j] = 1

		bounds = [ 0, 1, 2, 3 ]
		norm = colors.BoundaryNorm(bounds, cmap.N)
		
		fig, ax = plt.subplots()
		ax.imshow(filtered_policy, cmap=cmap, norm=norm)
		ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
		ax.set_xticks(np.arange(-0.5, nb_cols, 1))
		ax.set_yticks(np.arange(-0.5, nb_rows, 1))
		
		# Indicate arrows on grid
		for i in range(nb_rows):
			for j in range(nb_cols):
				state = i * nb_cols + j
				action = receiver.act(state, [ message ], False)
				action_str = ''

				if (action == 0): action_str = '↓' # Down
				if (action == 1): action_str = '→' # Right
				if (action == 2): action_str = '↑' # Up
				if (action == 3): action_str = '←' # Left

				ax.text(j,i,action_str,ha='center',va='center')

if __name__ == '__main__':
	worlds = {
			#"empty": Empty_room,
			"pong": Pong,
			#"four_room": Four_room,
			#"two_room": Two_room,
			#"flower": Flower, 
   		}
	M = 1
	for env_name, world in worlds.items(): 
		environment = world()

		best_eval_rewards = False
		best_senders = False
		best_receiver = False
		best_avg_reward = 0

		for i in range(20):
			print(f"Run {i + 1}")
			eval_rewards, senders, receiver = run(
				M=M,
				C=4,
				learning_rate_s=1e-3,
				learning_rate_r=1e-3,
				gamma_s=0.9,
				gamma_r=0.9,
				epsilon_max_s=1,
				epsilon_max_r=1,
				epsilon_min_s=0.1,
				epsilon_min_r=0.1,
				epsilon_decay_s=0.99999,
				epsilon_decay_r=0.9999,
				warmup_s=0,
				warmup_r=0,
				environment=environment,
				nb_outer_episodes=100,
				nb_inner_episodes=1000,
				nb_evaluation_episodes=20
			)

			avg_reward = np.average(eval_rewards)
			if (avg_reward > best_avg_reward):
				best_eval_rewards = eval_rewards
				best_senders = senders
				best_receiver = receiver
				best_avg_reward = avg_reward

		print("Done, saving ...")
		evaluate_every = 6000//300
		eval_rewards2 = [ [(i+1)*100, best_eval_rewards[i]] for i in range(len(best_eval_rewards)) if (i+1) % evaluate_every == 0 ]
		DataFrame(np.array(eval_rewards2), columns=[
				"Step", "Average reward"]).to_csv("results_SARSA_3_"+env_name)



		# Only works for one sender
		if (M == 1):
			policy = display_sender_policy(best_senders[0], environment)
			display_receiver_policy(best_receiver, policy)

		# Works for multiple senders
		if (M > 1):
			for sender in best_senders:
				display_sender_policy(sender, environment)
	
		# Print receiver actions
		print(f"######## {env_name} ########")
		env = world()
		for i in range(10):
			print(f"-------- {i} --------")

			done = False
			state = env.reset()

			goal = env.goal()
			messages = [ sender.act(goal, False) for sender in best_senders ]

			action = best_receiver.greedy_action(state, messages)

			env.print()
				
			i = 0
			while not done:
				reward, next_state, done = env.step(action)
				next_action = best_receiver.greedy_action(next_state, messages)

				print()
				env.print()

				state = next_state
				action = next_action

				i += 1
				if (i > 20): break
	
	

		plt.figure()
		plt.plot(range(len(best_eval_rewards)),best_eval_rewards)
		plt.ylabel("Return")
		plt.show()