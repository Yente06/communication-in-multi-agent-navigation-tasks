import numpy as np

"""
        # [0,0] -> 0
        # [1,0] -> 1
        # [2,0] -> 2
        # [0,1] -> 3
        # [1,1] -> 4
        # [2,1] -> 5
        # [0,2] -> 6
        # [1,2] -> 7
        # [2,2] -> 8
c = 0
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                message_val = 0
                arr = [l,k,j,i]
                for x, m in enumerate(arr):
                    message_val += m * 3**x
                print(f"{arr} -> {message_val} should be {c}")
                c += 1
"""              


class Receiver:
    def __init__(
        self,
        M,
        N,
        nb_states,
        nb_actions,
        learning_rate,
        gamma,
        epsilon_max,
        epsilon_min,
        epsilon_decay,
        warmup):
        """
            M: Number of sender agents
            N: number of distinct messages per sender
            nb_states: Amount of possible states
            nb_actions: Amount of possible messages
            learning_rate: Action exploration rate
            gamma: Discount factor
            epsilon_max: Maximum epsilon of epsilon-greedy
            epsilon_min: Minimum epsilon of epsilon-greedy
            epsilon_decay: Decay epsilon of epsilon-greedy
        """
        self.M = M
        self.N = N
        self.nb_states = nb_states
        self.nb_actions = nb_actions
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.epsilon = epsilon_max
        self.epsilon_max = epsilon_max
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.warmup = warmup
        self.reset()

    def reset(self):
        self.epsilon = self.epsilon_max
        self.Q = np.zeros((self.nb_states, self.N**self.M, self.nb_actions)) # A Q-table per state

    def act(self, state, messages, training):
        
        if (training and (np.random.rand() <= self.epsilon or self.warmup > 0 )):
            # Choose randomly
            self.warmup = max(self.warmup - 1, 0)
            return np.random.randint(self.nb_actions)
        else:
            # Choose greedy
            return self.greedy_action(state, messages)

    def messages_to_idx(self, messages):
        message_val = 0
        for x, m in enumerate(messages):
            message_val += m * self.N**x

        return message_val
    
    def greedy_action(self, state, messages):
        message_idx = self.messages_to_idx(messages)
        return np.argmax(self.Q[state, message_idx])

    def learn(self, state, action, reward, next_state, next_action, messages, done):

        if (done): self.epsilon = max(self.epsilon_min, self.epsilon * self.epsilon_decay)

        message_idx = self.messages_to_idx(messages)

        #self.Q[state, message_idx] = [ rew * 0.99 for rew in self.Q[state, message_idx] ]
        
        #self.Q[state, message_idx, action] += self.learning_rate * ( reward + self.gamma * self.Q[next_state, message_idx, np.argmax(self.Q[next_state, message_idx])] - self.Q[state, message_idx, action] )
        self.Q[state, message_idx, action] += self.learning_rate * ( reward + self.gamma * self.Q[next_state, message_idx, next_action] - self.Q[state, message_idx, action] )