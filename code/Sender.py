import numpy as np

class Sender:
    def __init__(
        self,
        nb_states,
        nb_actions,
        learning_rate,
        gamma,
        epsilon_max,
        epsilon_min,
        epsilon_decay,
        warmup):
        """
            nb_states: Amount of possible states
            nb_actions: Amount of possible messages
            learning_rate: Action exploration rate
            gamma: Discount factor
            epsilon_max: Maximum epsilon of epsilon-greedy
            epsilon_min: Minimum epsilon of epsilon-greedy
            epsilon_decay: Decay epsilon of epsilon-greedy
        """

        self.nb_actions = nb_actions
        self.nb_states = nb_states
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.epsilon = epsilon_max
        self.epsilon_max = epsilon_max
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.warmup = warmup
        self.reset()

    def reset(self):
        self.epsilon = self.epsilon_max
        self.Q = np.zeros((self.nb_states, self.nb_actions)) # A Q-table per state
    
    def act(self, state, training):
        
        if (training and (np.random.rand() <= self.epsilon or self.warmup > 0 )):
            # Choose randomly
            self.warmup = max(self.warmup - 1, 0)
            return np.random.randint(self.nb_actions)
        else:
            # Choose greedy
            return self.greedy_action(state)

    
    def greedy_action(self, state):
        return np.argmax(self.Q[state])

    def learn(self, state, action, reward, done):

        if (done): self.epsilon = max(self.epsilon_min, self.epsilon * self.epsilon_decay)

        self.Q[state, action] += self.learning_rate * reward
