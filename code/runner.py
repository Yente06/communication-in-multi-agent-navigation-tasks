import numpy as np
import matplotlib.pyplot as plt

from Sender import Sender
from Receiver import Receiver
from Environment import Pong

def run_episode(receiver, environment, messages, p_term, training=True):
    done = False
    state = environment.state()
    action = receiver.act(state, messages, training) 
    while not done:
        reward, next_state, done = environment.step(action)
        next_action = receiver.act(next_state, messages, training)

        if(training): receiver.learn(state, action, reward, next_state, next_action, messages, done)
        state = next_state
        action = next_action

        if (done): return reward
        if (np.random.rand() <= p_term): return reward


def run(M,
        C,
        learning_rate_s,
        learning_rate_r,
        gamma_s,
        gamma_r,
        epsilon_max_s,
        epsilon_max_r,
        epsilon_min_s,
        epsilon_min_r,
        epsilon_decay_s,
        epsilon_decay_r,
        warmup_s,
        warmup_r,
        environment,
        nb_episodes,
        evaluate_every,
        nb_evaluation_episodes):
    """
        M: Number of sender agents
        C: Communication chanel capacity (N^M)
        learning_rate_s: Learning rate of senders
        learning_rate_r: Learning rate of receivers
        gamma_s: Sender's Q-learning discount factor
        gamma_r: Receiver's Q-learning discount factor
        epsilon_max_s: Sender's maximum action exploration rate
        epsilon_max_r: Receiver's maximum action exploration rate
        epsilon_min_s: Sender's minimum action exploration rate
        epsilon_min_r: Receiver's minimum action exploration rate
        epsilon_decay_s: Sender's action exploration rate decay
        epsilon_decay_r: Receiver's action exploration rate decay
        warmup_s: The amount of rounds for which the senders will select a rendom action in the beginning
        warmup_r: The amount of rounds for which the receiver will select a rendom action in the beginning
        environment: The envorinment
        nb_episodes_s: The total amount of episodes to train for
        evaluate_every: The evaluation interval (on the outer loop of the training process)
        nb_evaluation_episodes: Number of episodes per evaluation
    """

    nb_messages = int(C ** (1 / M))
    p_term = 1 - gamma_r

    senders = [ Sender(environment.num_states, nb_messages, learning_rate_s, gamma_s, epsilon_max_s, epsilon_min_s, epsilon_decay_s, warmup_s) for _ in range(M) ]
    receiver = Receiver(M, nb_messages, environment.num_states, 4, learning_rate_r, gamma_r, epsilon_max_r, epsilon_min_r, epsilon_decay_r, warmup_r)
    
    rewards = np.zeros(nb_episodes)
    eval_rewards = np.zeros(nb_episodes // evaluate_every)

    avg_reward = 0

    for episode in range(nb_episodes):
        environment.reset()
        goal = environment.goal()
        messages = [ sender.act(goal, True) for sender in senders ]

        # Train receiver
        reward = run_episode(receiver, environment, messages, p_term, True)

        # Train sender
        for i, sender in enumerate(senders):
            sender.learn(goal, messages[i], reward, True)

        avg_reward += reward
        rewards[episode] = reward

        # Print training rewards
        if (episode % evaluate_every == 0):
            # AVG trainig reward
            print(f"Average trainig reward: {avg_reward / evaluate_every}")
            avg_reward = 0

            # AVG evaluating reward
            eval_reward = 0
            for eval in range(nb_evaluation_episodes):
                environment.reset()
                goal = environment.goal()
                messages = [ sender.act(goal, False) for sender in senders ]
                eval_reward += run_episode(receiver, environment, messages, p_term, False)
                eval_rewards[episode // evaluate_every] = eval_reward
           
            print(f"Average evaluation reward: {eval_reward / nb_evaluation_episodes}")

    return rewards, eval_rewards, senders, receiver
        



if __name__ == '__main__':
    environment = Pong()
    rewards, eval_rewards, senders, receiver = run(
        M=1,
        C=4,
        learning_rate_s=5e-5,
        learning_rate_r=5e-5,
        gamma_s=0.9,
        gamma_r=0.9,
        epsilon_max_s=1.0,
        epsilon_max_r=1.0,
        epsilon_min_s=0.1,
        epsilon_min_r=0.1,
        epsilon_decay_s=0.99999,
        epsilon_decay_r=0.99999,
        warmup_s=0,
        warmup_r=0,
        environment=environment,
        nb_episodes=1000000,
        evaluate_every=10000,
        nb_evaluation_episodes=100
    )



    policy = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            policy[i,j] = senders[0].act(i*5 + j, False)

    print(senders[0].Q)
    print(policy)

    plt.figure()
    plt.plot(range(len(eval_rewards)),eval_rewards)
    plt.ylabel("Return")
    plt.show()